/// <reference types="cypress" />

import {ConsumersPage} from '../page_objects/consumers'
import { Result } from '../page_objects/result'
import { SearchResultPage } from '../page_objects/search_result'

const consumersPage = ConsumersPage.getInstance()

export const result = Result.getInstance()

export let searchResultPage: SearchResultPage

export const goToPage = () => {
    consumersPage.go()
}

export const searchBusinessOrService = (business: string, location: string) =>{
    searchResultPage = consumersPage.typeBusiness(business)
                                    .typeLocation(location)
                                    .clickOnSearchButton()
}