import { Given, When, Then } from 'cypress-cucumber-preprocessor/steps';
import { PickATime } from '../../page_objects/pickATime';
import { Result } from '../../page_objects/result';
import * as myTimeWorkflow from '../../workflows/mytime'

const resultPage = Result.getInstance()
const pickATimePage = PickATime.getInstance()

let chosenServiceType: string

//GIVEN

Given('I am in the Consumers page', () => {
  myTimeWorkflow.goToPage()
});

//WHEN

When('I search for {string} in {string}', (business: string, location: string) => {
  myTimeWorkflow.searchBusinessOrService(business, location)
});

When('I open business with name {string}', (resultName: string) => {
  myTimeWorkflow.searchResultPage.clickOnChosenResult(resultName)
});

When('I select {string} in the filter from the left panel', (filterName: string) => {
  resultPage.clickOnFilter(filterName)
});

When('I select second staff from the staff filter in the left side panel', () => {
  resultPage.selectSecondOptionInTheStaff()
});

When('I click in the Book button for the {string} service', (serviceType: string) => {
  resultPage.book(serviceType)
  chosenServiceType = serviceType
});


//THEN

Then('I verify the multiple results shown, at least {string}', (expectedLength: string) => {
  myTimeWorkflow.searchResultPage.resultContentShouldBeGraterThan(parseInt(expectedLength))
});

Then('I should be on the next screen', () => {
  expect(myTimeWorkflow.result).to.be.ok
});

Then('I verify that the user is presented with a list of available time slots with at least {string} entries', (expectedOpenTimes: string) => {
  pickATimePage.assertNumberOfOpenTime(parseInt(expectedOpenTimes))
});

Then('I verify that the service displayed in the right side panel is the one selected in the step before', () => {
  pickATimePage.assertServiceType(chosenServiceType)
});

Then('I verify that the service price displayed in the right side panel is the one in the step before', () => {
  const chosenServicePrice = Result.chosenServicePrice
  pickATimePage.assertPrice(chosenServicePrice)
});

Then('I verify that the staff displayed in the right side panel is the one chosen before', () => {
  const selectedService = Result.selectedStaff
  pickATimePage.assertSelectedStaff(selectedService)
});
