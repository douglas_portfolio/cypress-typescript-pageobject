Feature: MyTime Searching
As an user
I want to search for a service
So that, I should be able to schedule it

Scenario: Schedule a time for haircut
    Given I am in the Consumers page
    When I search for "haircut" in "San Francisco, CA"
    Then I verify the multiple results shown, at least "3"
    When I open business with name "Test - Skylar Copy 1"
    Then I should be on the next screen
    When I select "All services" in the filter from the left panel
    When I select second staff from the staff filter in the left side panel
    When I click in the Book button for the "Men's Haircut" service
    Then I verify that the user is presented with a list of available time slots with at least "2" entries
    Then I verify that the service displayed in the right side panel is the one selected in the step before
    Then I verify that the service price displayed in the right side panel is the one in the step before
    Then I verify that the staff displayed in the right side panel is the one chosen before
    