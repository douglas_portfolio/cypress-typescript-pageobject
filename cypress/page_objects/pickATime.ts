// @ts-check
/// <reference types="cypress-xpath" />

export class PickATime {

    private static instance : PickATime

    private constructor () {}

    public static getInstance = () : PickATime => {
        if(!PickATime.instance)
        PickATime.instance = new PickATime()
        return PickATime.instance
    }

    assertNumberOfOpenTime = (expectedOpenTimes : number) => {
        cy.get('.opentime-title').should('have.length.greaterThan', expectedOpenTimes)
    }

    assertSelectedStaff = (expectedSelectedStaff: string) =>{
        cy.get('tbody > :nth-child(1) > .text-3').should("have.text", expectedSelectedStaff)
    }

    assertPrice = (expectedPrice: string) => {
        cy.get('.variation-price > .normal-price').should('have.text', expectedPrice)
    }

    assertServiceType = (expectedServiceType: string) => {
        cy.get('.variation-name').should('have.text', expectedServiceType)
    }

}