/// <reference types="cypress" />

import { Result } from "./result"

export class SearchResultPage {

    private static instance : SearchResultPage

    private constructor () {}

    public static getInstance = () : SearchResultPage => {
        if(!SearchResultPage.instance)
            SearchResultPage.instance = new SearchResultPage()
        return SearchResultPage.instance
    }

    public resultContentShouldBeGraterThan = (expectedLength) => {
        cy.get('.search-result-content').should('have.length.greaterThan', expectedLength)
    }

    public clickOnChosenResult = (resultName: string) : Result => {
        cy.contains(resultName).click()
        return Result.getInstance()
    }
}