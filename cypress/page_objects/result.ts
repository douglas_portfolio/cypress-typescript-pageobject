// @ts-check
/// <reference types="cypress-xpath" />

export class Result {

    private static instance : Result
    public static selectedStaff: string
    public static chosenServicePrice: string

    private constructor () {}

    public static getInstance = () : Result => {
        if(!Result.instance)
        Result.instance = new Result()
        return Result.instance
    }

    clickOnFilter = (filterName: string) => {
        cy.contains(filterName).click()
        return Result.getInstance()        
    }

    selectSecondOptionInTheStaff = () : Result => {
        const selectorLocator = '.Select-placeholder'
        cy.get(selectorLocator).click()
        cy.get(selectorLocator).type('{downarrow}')
        cy.get(selectorLocator).type('{enter}')
        cy.get('.selected-breed').invoke('text').then((text) => Result.selectedStaff = text.toString().split('change')[0])
        return Result.getInstance()
    }

    book = (serviceType :string) : Result =>{
        require('cypress-xpath')
        cy.xpath(`//*[contains(text(), "${serviceType}")]//following::button[1]`).click()
        cy.contains('Select Time').click()
        cy.xpath(`//*[contains(text(), "Men's Haircut")]//following::span[@class='normal-price']`).invoke('text')
          .then((text) => Result.chosenServicePrice = text.toString())
        return Result.getInstance()
    }

}