/// <reference types="cypress" />

import {SearchResultPage} from '../page_objects/search_result'

export class ConsumersPage {

    private static instance : ConsumersPage

    private constructor() { }

    public static getInstance() : ConsumersPage {
        if (!ConsumersPage.instance)
            ConsumersPage.instance = new ConsumersPage()
        return ConsumersPage.instance
    }

    public go = () : ConsumersPage => {
        cy.visit('https://www.mytime.com/consumers')
        return ConsumersPage.getInstance()
    }

    public acceptCookies = () : ConsumersPage => {
        cy.get('#accept-cookies-and-close-button').click()
        return ConsumersPage.getInstance()
    }

    public typeBusiness = (business : string) : ConsumersPage => {
        cy.get('.search-container > .business-search > #search-query').type(business)
        return ConsumersPage.getInstance()
    }

    public typeLocation = (location : string ) : ConsumersPage => {
        cy.get('.location-search > #search-location').type(location)
        return ConsumersPage.getInstance()
    }

    public clickOnSearchButton = () => { 
        cy.get('.flat-blue-btn').click()
        return SearchResultPage.getInstance()
    }

}