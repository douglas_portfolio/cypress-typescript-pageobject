# cypress-cucumber-typescript-example
Example of using Cypress with Cucumber and TypeScript

All the configuration is in [cypress/plugins/index.js](cypress/plugins/index.js)

TypeScript step definitions are in [cypress/integration/passWithTypescript](cypress/integration/passWithTypescript)

(I'm using the nonGlobalStepDefinitions configuration in the [package.json](package.json) )


# To run this project

If you want to run this project using cypress open and debug, just run:

```
node run open
```

If you want to run this project, please run:

```
node run test
```